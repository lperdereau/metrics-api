const hostnamesController = require('./controller')

module.exports = [{
    method: "GET",
    path: "/hostnames",
    handler: hostnamesController.getHostnames,
    options: {
      description: "Get Hostnames",
      notes: "Returns a collection of hostnames",
      tags: ["api"],
    }
  }]