const Joi = require("@hapi/joi");

module.exports = {
  getMetricsFromHostname: {
    params: {
        hostname: Joi.string()
        .required()
        .description("The hostname"),
        nb_entry: Joi.number()
        .positive()
        .min(0)
        .max(20)
    }
  },
  postMetricsFromHostname: {
    payload: {
      cpu_load: Joi.number()
        .max(100)
        .positive()
        .description("The processor loading in percentage"),
      ram_usage: Joi.number()
        .max(100)
        .positive()
        .description("The memory loading in percentage"),
      free_storage_left: Joi.number()
        .positive()
        .description("The free storage left on the host in Gb"),
      total_storage_size: Joi.number()
        .positive()
        .description("The total storage size on the host in Gb"),
      created_at: Joi.string()
        .isoDate()
        .description("The metrics's pick up date"),
    }
  }
}