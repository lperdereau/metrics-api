const mongo = require('../mongo');

function getMetricsFromHostname(hostname, nb_entry) {
  const limit = nb_entry || 20;
  return mongo.CLIENT.then(db => {
      return db.db(mongo.DB)
      .collection("metrics")
      .find({ hostname: hostname })
      .sort({ timestamp: -1 })
      .limit(parseInt(limit))
      .toArray().catch( err => {
          console.log(err);
      });
  }).catch(err => {
    console.log(err);
  });
}

function insertMetrics(data){
  const metrics = data
  return mongo.CLIENT.then(db => {
    return db.db(mongo.DB).collection("metrics").insertOne(metrics).catch( err => {
      console.log(err)
    });
  });
}

module.exports = {
  handleGetMetrics: async (request, h) => {
    return getMetricsFromHostname(request.params.hostname, request.query.nb_entry)
  },
  handlePostMetrics: (request, h) => {
    const data = {
      hostname: request.params.hostname,
      cpu_load: request.payload.cpu_load,
      ram_usage: 100-request.payload.ram_usage,
      free_storage_left: request.payload.free_storage_left,
      total_storage_size: request.payload.total_storage_size,
      timestamp: request.payload.created_at
    }
    return insertMetrics(data)
  }
}